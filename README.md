# ffmpeg_h264_H3

# kernel 4.20以上
[armbian](https://dl.armbian.com/orangepipc/Debian_stretch_next.7z)を入手して使う(bionicはだめだった)

```sh
sudo apt update
sudo apt upgrade -y
sudo armbian-config # System -> Other -> 新しいカーネル
```

# インストール

依存パッケージ

```sh
sudo apt install libpulse-dev libv4l-dev libmp3lame-dev libx264-dev
sudo apt install libavcodec-ffmpeg-extra56 libavdevice-ffmpeg56 libavformat-ffmpeg56 # 多分必要
```

ビルド

```sh
git clone git@gitlab.com:kmchu/ffmpeg_h264_h3.git
cd ffmpeg_h264_h3
export CXXFLAGS="$CXXFLAGS -fPIC"
./configure --prefix=/usr --enable-nonfree --enable-gpl --enable-version3 --enable-vdpau --enable-libx264 --enable-libmp3lame --enable-libpulse --enable-libv4l2 --enable-pic --enable-shared
make -j 4
sudo make install
```

# 使う
```sh
ffmpeg -f v4l2 -video_size 1280x720 -i /dev/video0 -pix_fmt nv12 -r 25 -c:v cedrus264 -vewait 5 -qp 30 -t 60 -f mp4 test.mp4 -y
```

# 参考
ソースの元  
https://github.com/uboborov/ffmpeg_h264_H3  
https://github.com/stulluk/FFmpeg-Cedrus  


ビルドの参考  
https://forum.armbian.com/topic/1842-ffmpeg-with-cedrus-h264-hw-encoder-h3-cmos-camera/  
https://stackoverflow.com/questions/25539034/opencv-make-fails-recompile-with-fpic  
https://askubuntu.com/questions/984957/blender-missing-library-libavcodec-so-54-16-04  
https://stackoverflow.com/questions/13812185/how-to-recompile-with-fpic  

